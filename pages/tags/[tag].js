import { useRouter } from "next/router";
import Link from "next/link";
import useSWR from "swr";

import Gallery from "../../components/Gallery";

export default function TagId() {
  const router = useRouter();
  const { tag } = router.query;

  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data: images } = useSWR("/api/images/tags/" + tag, fetcher);

  return (
    <section>
      <section className="flex py-4">
        <Link href="/">
          <a className="flex px-3 py-1.5 bg-gray-200 font-medium rounded uppercase text-sm">
            {tag}
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5 ml-1"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                clipRule="evenodd"
              />
            </svg>
          </a>
        </Link>
      </section>
      <Gallery images={images} />
    </section>
  );
}

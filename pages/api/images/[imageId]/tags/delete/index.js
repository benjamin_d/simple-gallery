import cloudinary from "../../../../../../utils/cloudinaryConfig";

export default async function handler(req, res) {
  const { imageId } = req.query;
  const results = await cloudinary.uploader.remove_all_tags(imageId);
  res.status(200).json(results);
}

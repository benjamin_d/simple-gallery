import cloudinary from "../../../../../../utils/cloudinaryConfig";

export default async function handler(req, res) {
  const { imageId, newTags } = req.query;
  const results = await cloudinary.uploader.replace_tag(newTags, imageId);
  res.status(200).json(results);
}

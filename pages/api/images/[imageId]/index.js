import cloudinary from "../../../../utils/cloudinaryConfig";

export default async function handler(req, res) {
  const { imageId } = req.query;
  const resources = await cloudinary.api.resources_by_ids([imageId], {
    tags: true,
  });
  res.status(200).json(resources);
}

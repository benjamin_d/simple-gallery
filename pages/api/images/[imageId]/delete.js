import cloudinary from "../../../../utils/cloudinaryConfig";

export default async function handler(req, res) {
  const { imageId } = req.query;
  const resources = await cloudinary.uploader.destroy(imageId);
  res.status(200).json(resources);
}

import cloudinary from "../../../../utils/cloudinaryConfig";

export default async function handler(req, res) {
  const { tag } = req.query;
  const resources = await cloudinary.api.resources_by_tag(tag);
  res.status(200).json(resources);
}

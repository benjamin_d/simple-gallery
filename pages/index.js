import { useState } from "react";
import useSWR from "swr";

import UploadImageModal from "../components/UploadImageModal";
import Tags from "../components/Tags";
import Gallery from "../components/Gallery";
import PrimaryButton from "../components/Buttons/PrimaryButton";

export default function Home() {
  const [modalIsOpen, setModalIsOpen] = useState(false);

  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data: tags, error: tagErrors } = useSWR("/api/tags", fetcher);
  const { data: images, error: imageErrors } = useSWR("/api/images", fetcher);

  return (
    <>
      <PrimaryButton onClick={() => setModalIsOpen(true)} text="Upload image" />
      <UploadImageModal
        open={modalIsOpen}
        closeModal={() => setModalIsOpen(false)}
      />
      {tagErrors ? (
        "Error loading tags."
      ) : tags ? (
        <Tags tags={tags} />
      ) : (
        "No tags yet."
      )}
      {imageErrors ? (
        "Error loading images."
      ) : images ? (
        <Gallery images={images} />
      ) : (
        "No images yet."
      )}
    </>
  );
}

import { useState } from "react";
import { useRouter } from "next/router";
import useSWR from "swr";
import Image from "next/image";

import Tags from "../../components/Tags";
import EditTagsModal from "../../components/EditTagsModal";
import DeleteImageModal from "../../components/DeleteImageModal";
import BackButton from "../../components/Buttons/BackButton";
import SecondaryButton from "../../components/Buttons/SecondaryButton";

export default function ImagePage() {
  const [editModalIsOpen, setEditModalIsOpen] = useState(false);
  const [deleteModalIsOpen, setDeleteModalIsOpen] = useState(false);

  const router = useRouter();
  const { id } = router.query;

  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data: image } = useSWR("/api/images/" + id, fetcher);

  return (
    <>
      <section className="flex justify-between py-4">
        <BackButton />
        <SecondaryButton
          onClick={() => setEditModalIsOpen(true)}
          text="Edit tags"
        />
      </section>
      {image && (
        <section>
          <section className="text-center sm:py-4">
            <Image
              src={image.resources[0].secure_url}
              alt={image.resources[0].public_id}
              width={image.resources[0].width}
              height={image.resources[0].height}
            />
          </section>

          <Tags tags={image.resources[0].tags} />

          <button
            className="w-full border border-red-600 text-red-600 p-3 font-medium rounded my-20"
            onClick={() => setDeleteModalIsOpen(true)}
          >
            Delete image
          </button>

          <EditTagsModal
            open={editModalIsOpen}
            closeModal={() => setEditModalIsOpen(false)}
            tags={image.resources[0].tags}
            imageId={id}
          />

          <DeleteImageModal
            open={deleteModalIsOpen}
            closeModal={() => setDeleteModalIsOpen(false)}
            id={image.resources[0].public_id}
          />
        </section>
      )}
    </>
  );
}

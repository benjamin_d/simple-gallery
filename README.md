# SimpleGallery

SimpleGallery is a website where users can upload and view their images.

## Tech stack

- Next.js
- TailwindCSS
- Cloudinary

## Prerequisites

This project uses [cloudinary](https://cloudinary.com/) as the backend. You need a cloudinary API key to use the image functionalities.

## Getting started

Create a `.env.local` file with the following variables and enter your API details.

```
NEXT_PUBLIC_CLOUDINARY_CLOUD={your_api_detail}
CLOUDINARY_API_KEY={your_api_detail}
CLOUDINARY_API_SECRET={your_api_detail}
```

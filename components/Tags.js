import Link from "next/link";

export default function Tags({ tags }) {
  return (
    <section className="py-4">
      <ul className="flex overflow-x-auto">
        {tags?.map((tag, index) => (
          <li key={tag + index} className="mr-3">
            <Link href={`/tags/${tag}`}>
              <a className="flex truncate px-3 py-1.5 bg-gray-200 font-medium rounded uppercase text-sm">
                {tag}
              </a>
            </Link>
          </li>
        ))}
      </ul>
    </section>
  );
}

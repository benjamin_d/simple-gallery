import Image from "next/image";
import Link from "next/link";

export default function Gallery({ images }) {
  return (
    <section className="sm:grid sm:grid-cols-3 sm:gap-8 text-center items-center">
      {images?.resources.map((image) => (
        <Link href={`/${image.public_id}`} key={image.public_id}>
          <a>
            <Image
              src={image.secure_url}
              width={image.width}
              height={image.height}
              alt={image.public_id}
            />
          </a>
        </Link>
      ))}
    </section>
  );
}

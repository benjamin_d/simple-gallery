export default function SecondaryButton({ onClick, text }) {
  return (
    <button
      className="border border-black h-12 px-3 font-medium rounded"
      onClick={onClick}
    >
      {text}
    </button>
  );
}

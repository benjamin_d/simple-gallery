export default function PrimaryButton({ onClick, text }) {
  return (
    <button
      className="w-full bg-black text-white px-3 h-12 font-medium rounded"
      onClick={onClick}
    >
      {text}
    </button>
  );
}

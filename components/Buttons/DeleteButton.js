export default function DeleteButton({ onClick, text }) {
  return (
    <button
      className="w-full bg-red-700 text-white h-12 font-medium rounded"
      onClick={onClick}
    >
      {text}
    </button>
  );
}

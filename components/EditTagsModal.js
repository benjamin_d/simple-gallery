import { useState } from "react";
import { useSWRConfig } from "swr";

import ModalContainer from "./ModalContainer";
import ModalHeader from "./ModalHeader";
import ModalFooter from "./ModalFooter";
import PrimaryButton from "./Buttons/PrimaryButton";
import SecondaryButton from "./Buttons/SecondaryButton";

export default function EditTagsModal({ open, closeModal, tags, imageId }) {
  const [newTags, setNewTags] = useState(tags);

  const { mutate } = useSWRConfig();

  async function handleSaveClick() {
    try {
      if (newTags.length < 1) {
        await fetch(`/api/images/${imageId}/tags/delete/`);
      } else {
        await fetch(`/api/images/${imageId}/tags/replace/${newTags}`);
      }
    } catch (error) {
      console.log(error);
    } finally {
      mutate(`/api/images/${imageId}`);
      closeModal();
    }
  }

  function handleCancelClick() {
    setNewTags(tags);
    closeModal();
  }

  function addTag(event) {
    if (event.key === "Enter") {
      if (event.target.value.length > 0) {
        setNewTags([...newTags, event.target.value]);
        event.target.value = "";
      }
    }
  }

  function removeTag(_tag) {
    const filteredTags = newTags.filter((tag) => tag !== _tag);
    setNewTags(filteredTags);
  }

  if (!open) return null;

  return (
    <>
      <ModalContainer>
        <ModalHeader heading="Edit tags" />
        <ul className="flex flex-wrap pb-2">
          {newTags.map((tag) => (
            <li key={tag} className="mr-2 mb-2">
              <button
                className="flex truncate px-3 py-1.5 bg-gray-200 font-medium rounded uppercase text-sm"
                onClick={() => removeTag(tag)}
              >
                {tag}
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-5 w-5 ml-1"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fillRule="evenodd"
                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  />
                </svg>
              </button>
            </li>
          ))}
        </ul>
        <input
          onKeyDown={addTag}
          placeholder="Enter tag..."
          className="w-full border border-black rounded p-2"
        />
        <span className="text-sm py-4 mb-8">
          Press
          <kbd className="mx-1 px-1 py-1 border shadow bg-neutral-50 rounded">
            Enter
          </kbd>
          to confirm.
        </span>
        <ModalFooter>
          <PrimaryButton onClick={handleSaveClick} text="Save" />
          <SecondaryButton onClick={handleCancelClick} text="Cancel" />
        </ModalFooter>
      </ModalContainer>
    </>
  );
}

import { useState, useRef } from "react";
import { useSWRConfig } from "swr";

import ModalContainer from "./ModalContainer";
import ModalHeader from "./ModalHeader";
import ModalFooter from "./ModalFooter";
import PrimaryButton from "./Buttons/PrimaryButton";
import SecondaryButton from "./Buttons/SecondaryButton";
import LoadingSpinner from "./LoadingSpinner";

export default function UploadImageModal({ open, closeModal }) {
  const [image, setImage] = useState();
  const [enableAutoTagging, setEnableAutoTagging] = useState(false);
  const [isUploading, setIsUploading] = useState(false);

  const { mutate } = useSWRConfig();

  const fileRef = useRef();

  function handleOnChange(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = () => {
        setImage(reader.result);
      };

      reader.readAsDataURL(event.target.files[0]);
    }
  }

  function handleCheckboxChange() {
    setEnableAutoTagging(!enableAutoTagging);
  }

  async function handleOnSubmit(event) {
    event.preventDefault();
    setIsUploading(true);

    try {
      if (enableAutoTagging) {
        const data = await fetch("/api/images/uploadAutoTagging", {
          method: "POST",
          body: JSON.stringify({
            image: image,
          }),
        }).then((r) => r.json());
      } else {
        const data = await fetch("/api/images/upload", {
          method: "POST",
          body: JSON.stringify({
            image: image,
          }),
        }).then((r) => r.json());
      }
    } catch (error) {
      console.log(error);
    } finally {
      setImage(null);
      mutate("/api/images");
      mutate("/api/tags");
      setIsUploading(false);
      closeModal();
    }
  }

  function handleCancelClick() {
    setImage(null);
    closeModal();
  }

  if (!open) return null;

  return (
    <>
      <ModalContainer>
        <ModalHeader heading="Upload image" />
        {isUploading ? (
          <span className="flex justify-center items-center font-medium py-8">
            <LoadingSpinner />
            Uploading...
          </span>
        ) : (
          <form
            method="post"
            onChange={handleOnChange}
            onSubmit={handleOnSubmit}
          >
            <input type="file" ref={fileRef} name="file" hidden />

            {image && (
              <section className="pb-8">
                <label className="flex justify-between items-center">
                  <span className="flex font-medium">Enable Auto Tagging</span>
                  <input
                    className="w-5 h-5"
                    type="checkbox"
                    checked={enableAutoTagging}
                    onChange={handleCheckboxChange}
                    disabled={isUploading}
                  />
                </label>
              </section>
            )}

            <button
              type="button"
              onClick={() => fileRef.current.click()}
              className="w-full py-8 rounded border-dashed border border-black mb-4"
            >
              {image && (
                <section>
                  <section className="flex flex-col items-center px-4">
                    <img src={image} width="180" className="rounded" />
                  </section>
                </section>
              )}

              {!image && (
                <span className="flex justify-center">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-6 mr-2"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth={2}
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12"
                    />
                  </svg>
                  Select image
                </span>
              )}
            </button>

            <ModalFooter>
              {image && <PrimaryButton text="Upload" />}
              <SecondaryButton onClick={handleCancelClick} text="Cancel" />
            </ModalFooter>
          </form>
        )}
      </ModalContainer>
    </>
  );
}

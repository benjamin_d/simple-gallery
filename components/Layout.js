import Head from "next/head";
import Link from "next/link";

export default function Layout({ children }) {
  return (
    <>
      <Head>
        <title>SimpleGallery</title>
        <meta name="description" content="A simple gallery." />
        <link rel="icon" href="/favicon.svg" />
      </Head>
      <section className="p-2 flex flex-col min-h-screen max-w-screen-lg mx-auto">
        <header className="flex py-2 sm:py-4">
          <nav>
            <Link href="/">
              <a className="flex items-center">
                <span className="text-3xl">🌄</span>
                <span className="font-bold text-xl ml-2">SimpleGallery</span>
              </a>
            </Link>
          </nav>
        </header>
        <main className="flex-1">{children}</main>
        <footer className="text-center py-8">
          <span>SimpleGallery, {new Date().getFullYear()}.</span>
        </footer>
      </section>
    </>
  );
}

export default function ModalHeader({ heading, text }) {
  return (
    <section className="mb-4">
      <h1 className="py-2 font-bold text-lg">{heading}</h1>
      <section className="">{text}</section>
    </section>
  );
}

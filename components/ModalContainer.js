export default function ModalContainer({ children }) {
  return (
    <>
      <section className="flex justify-center items-center fixed inset-0 z-50">
        <section className="w-full max-w-lg px-1">
          <section className="flex flex-col bg-white rounded p-4">
            {children}
          </section>
        </section>
      </section>
      <section className="opacity-80 fixed inset-0 z-40 bg-black" />
    </>
  );
}

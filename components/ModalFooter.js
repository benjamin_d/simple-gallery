export default function ModalFooter({ children }) {
  return <section className="grid gap-2 py-2">{children}</section>;
}

import { useRouter } from "next/router";

import ModalContainer from "./ModalContainer";
import ModalHeader from "./ModalHeader";
import ModalFooter from "./ModalFooter";
import DeleteButton from "./Buttons/DeleteButton";
import SecondaryButton from "./Buttons/SecondaryButton";

export default function DeleteImageModal({ open, closeModal, id }) {
  const router = useRouter();

  async function handleDelete(id) {
    try {
      await fetch(`/api/images/${id}/delete`);
    } catch (error) {
      console.log(error);
    } finally {
      router.back();
    }
  }

  if (!open) return null;

  return (
    <>
      <ModalContainer>
        <ModalHeader
          heading="Delete image"
          text="This image will be deleted permanently."
        />
        <ModalFooter>
          <DeleteButton onClick={() => handleDelete(id)} text="Delete image" />
          <SecondaryButton onClick={closeModal} text="Cancel" />
        </ModalFooter>
      </ModalContainer>
    </>
  );
}
